public class WordStatistics {
    public static void main(String[] args) {
        // Перевірка наявності аргумента командного рядка
        if (args.length == 0) {
            System.out.println("Будь ласка, введіть стрічку як аргумент командного рядка.");
            return;
        }

        // Отримання стрічки з аргумента командного рядка
        String inputString = args[0];

        // Підрахунок кількості слів та символів
        int wordCount = 0;
        int charCount = 0;

        // Прапорець для вказання, чи зараз проходить слово
        boolean inWord = false;

        // Перебір кожного символу в стрічці
        for (int i = 0; i < inputString.length(); i++) {
            char currentChar = inputString.charAt(i);

            // Перевірка, чи символ є пробілом
            if (Character.isWhitespace(currentChar)) {
                inWord = false;
            } else {
                // Якщо зараз не проходить слово, то збільшуємо лічильник слів
                if (!inWord) {
                    wordCount++;
                    inWord = true;
                }
                // Збільшуємо лічильник символів
                charCount++;
            }
        }

        // Виведення результатів
        System.out.println("Кількість слів: " + wordCount);
        System.out.println("Середня кількість символів у словах: " + (wordCount > 0 ? (double) charCount / wordCount : 0));
        System.out.println("Загальна кількість символів: " + charCount);
    }
}