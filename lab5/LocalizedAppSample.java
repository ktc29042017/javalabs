import java.util.ResourceBundle;

public class LocalizedAppSample {

    public static void main(String[] args) {

        // Завантажуємо ресурси з файлу локалізації lab5.properties
        ResourceBundle bundle = ResourceBundle.getBundle("lab5");

        // Отримуємо значення за ключем HelloWorld
        String message = bundle.getString("HelloWorld");

        // Виводимо повідомлення на екран
        System.out.println(message);
    }
}
