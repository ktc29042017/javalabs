
import java.util.*;

public class Main {

    public static void main(String[] args) {
        demonstrateHashSet();
        demonstrateTreeSet();
        demonstrateTreeMap();
        demonstrateLinkedList();
        demonstrateArrayList();
        demonstrateQueue();
        demonstratePriorityQueue();
    }

    static void demonstrateHashSet() {
        System.out.println("HashSet Example:");
        HashSet<String> set = new HashSet<>();
        set.add("apple");
        set.add("banana");
        set.add("orange");
        System.out.println("Elements in HashSet: " + set);
        System.out.println();
    }

    static void demonstrateTreeSet() {
        System.out.println("TreeSet Example:");
        TreeSet<String> set = new TreeSet<>();
        set.add("banana");
        set.add("apple");
        set.add("orange");
        System.out.println("Elements in TreeSet: " + set);
        System.out.println();
    }

    static void demonstrateTreeMap() {
        System.out.println("TreeMap Example:");
        TreeMap<Integer, String> map = new TreeMap<>();
        map.put(3, "banana");
        map.put(1, "apple");
        map.put(2, "orange");
        System.out.println("Elements in TreeMap: " + map);
        System.out.println();
    }

    static void demonstrateLinkedList() {
        System.out.println("LinkedList Example:");
        LinkedList<String> list = new LinkedList<>();
        list.add("apple");
        list.add("banana");
        list.add("orange");
        System.out.println("Elements in LinkedList: " + list);
        System.out.println();
    }

    static void demonstrateArrayList() {
        System.out.println("ArrayList Example:");
        ArrayList<String> list = new ArrayList<>();
        list.add("apple");
        list.add("banana");
        list.add("orange");
        System.out.println("Elements in ArrayList: " + list);
        System.out.println();
    }

    static void demonstrateQueue() {
        System.out.println("Queue Example:");
        Queue<String> queue = new LinkedList<>();
        queue.add("apple");
        queue.add("banana");
        queue.add("orange");
        System.out.println("Elements in Queue: " + queue);
        System.out.println();
    }

    static void demonstratePriorityQueue() {
        System.out.println("PriorityQueue Example:");
        PriorityQueue<String> queue = new PriorityQueue<>();
        queue.add("banana");
        queue.add("orange");
        queue.add("apple");
        System.out.println("Elements in PriorityQueue: " + queue);
        System.out.println();
    }
}