public class Main {

    public static void main(String[] args) {
        // Створення об'єктів підпроцесів
        ProcessBuilder processBuilder1 = new ProcessBuilder("cmd", "/c", "echo", "Hello");
        ProcessBuilder processBuilder2 = new ProcessBuilder("cmd", "/c", "echo", "World");

        try {
            // Запуск першого підпроцесу
            Process process1 = processBuilder1.start();
            // Запуск другого підпроцесу
            Process process2 = processBuilder2.start();

            // Очікування завершення обох підпроцесів
            int exitCode1 = process1.waitFor();
            int exitCode2 = process2.waitFor();

            // Виведення повідомлення про завершення кожного підпроцесу
            System.out.println("Перший підпроцес завершився з кодом: " + exitCode1);
            System.out.println("Другий підпроцес завершився з кодом: " + exitCode2);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}